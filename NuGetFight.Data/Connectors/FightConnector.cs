﻿using System;
using System.Configuration;
using System.Linq;
using MongoDB.Driver;
using NuGetFight.Data.Models;

namespace NuGetFight.Data.Connectors
{
    public class FightConnector
    {
        private MongoServer server;
        private MongoDatabase mongoDb;
        private MongoCollection<Fight> fightCollection;

        public FightConnector()
        {
            server = MongoServer.Create(ConfigurationManager.ConnectionStrings["MongoLab"].ToString());
            server.Connect();

            mongoDb = server.GetDatabase(ConfigurationManager.AppSettings["MongoDBName"].ToString());
        }

        public int GetBodyCount()
        {
            return Convert.ToInt32(mongoDb.GetCollection<Fight>("fights").Count());
        }

        public string GetMostSearchedPackage()
        {
            fightCollection = mongoDb.GetCollection<Fight>("fights");

            if (fightCollection.Count().Equals(0))
            {
                return String.Empty;
            }

            var fights = fightCollection.FindAll();
            var allQueries = fights.Select(fight => fight.Query1).ToList();
            allQueries.AddRange(fights.Select(fight => fight.Query2));

            var groupedQueries = allQueries.GroupBy(fight => fight).Select(g => new FightCount(g.Key, g.Count()));
            return groupedQueries.OrderByDescending(item => item.Count).First().Query;
        }

        public void SaveFight(string query1, string query2, DateTime date, string source, string sourceUrl = "")
        {
            fightCollection = mongoDb.GetCollection<Fight>("fights");
            fightCollection.Insert(Fight.CreateFight(query1, query2, date, source, sourceUrl));
        }

        
    }
}