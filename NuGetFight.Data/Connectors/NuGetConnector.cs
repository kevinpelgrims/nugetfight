﻿using System;
using System.Collections.Generic;
using System.Linq;
using NuGetFight.Data.Models;
using NuGetFight.Data.NuGetApi;
using NuGetFight;

namespace NuGetFight.Data.Connectors
{
    public class NuGetConnector
    {
        private FeedContext_x0060_1 context;
        public const string NuGetConnectionString = "http://www.nuget.org/api/v2/";
        public const string ChocolateyConnectionString = "http://chocolatey.org/api/v2/";
        public const string MyGetConnectionString = "http://www.myget.org/F/msdnlive/";

        public NuGetConnector(string connectionString)
        {
            context = new FeedContext_x0060_1(new Uri(connectionString));
        }

        public ICollection<V2FeedPackage> GetPackages(string packageName)
        {

            var packages = context.Packages.Where(p => (p.Id.ToUpper().Contains(packageName.ToUpper()) || p.Title.ToUpper().Contains(packageName)) && p.IsLatestVersion).OrderByDescending(p => p.DownloadCount).ToList();
            return packages;
        }

        public int GetDownloadCount(IEnumerable<V2FeedPackage> packages)
        {
            return packages.Sum(package => package.DownloadCount);
        }

        public ICollection<NuGetPackage> GetTopPackages(IEnumerable<V2FeedPackage> packages, int count)
        {
            return packages
                .OrderByDescending(p => p.DownloadCount)
                .Take(count)
                .Select(package => new NuGetPackage()
                {
                    Id = package.Id,
                    ProjectUrl = package.ProjectUrl,
                    GalleryUrl = package.GalleryDetailsUrl,
                    Title = String.IsNullOrEmpty(package.Title) ? package.Id : package.Title,
                    Version = package.Version,
                    Description = package.Description
                })
                .ToList();
        }
    }
}
