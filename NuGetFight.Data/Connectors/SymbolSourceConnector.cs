﻿using System.Net;
using NuGetFight.Data.Models;

namespace NuGetFight.Data.Connectors
{
    public class SymbolSourceConnector
    {
        public SymbolSourceResponse GetSymbolSourceResponse(string packageName, string packageVersion)
        {
            string symbolSourceUrl = string.Format("http://www.symbolsource.org/Public/Metadata/NuGet/Project/{0}/{1}",
                                                   packageName, packageVersion);
            HttpWebResponse symbolSourceResponse = null;
            HttpWebRequest symbolSourceRequest =
                (HttpWebRequest)WebRequest.Create(symbolSourceUrl);

            try
            {
                symbolSourceResponse = (HttpWebResponse)symbolSourceRequest.GetResponse();
            }
            catch (WebException webException)
            {
                if (webException.Response != null)
                {
                    symbolSourceResponse = (HttpWebResponse)webException.Response;
                }
                else
                {
                    throw;
                }
            }

            return new SymbolSourceResponse()
                       {
                           WebResponse = symbolSourceResponse,
                           PackageUrl = symbolSourceUrl,
                       };
        }
    }
}
