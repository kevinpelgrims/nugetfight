﻿namespace NuGetFight.Data.Models
{
    public class FightSources {
        public const string MyGet = "MyGet";
        public const string NuGet = "NuGet";
        public const string Chocolatey = "Chocolatey";
    }
}