﻿namespace NuGetFight.Data.Models
{
    public struct FightCount
    {
        public string Query { get; set; }
        public int Count { get; set; }

        public FightCount(string query, int count):this() {
            Query = query;
            Count = count;
        }
    }
}