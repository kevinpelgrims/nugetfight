﻿using System.Net;

namespace NuGetFight.Data.Models
{
    public class SymbolSourceResponse
    {
        public HttpWebResponse WebResponse { get; set; }
        public bool PackageExists { get { return WebResponse.StatusCode == HttpStatusCode.OK; } }
        public string PackageUrl { get; set; }
    }
}
