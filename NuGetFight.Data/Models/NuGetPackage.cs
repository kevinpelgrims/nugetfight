﻿namespace NuGetFight.Data.Models
{
    public class NuGetPackage
    {
        public string Id { get; set; }
        public string ProjectUrl { get; set; }
        public string GalleryUrl { get; set; }
        public string Title { get; set; }
        public string Version { get; set; }
        public string Description { get; set; }
    }
}