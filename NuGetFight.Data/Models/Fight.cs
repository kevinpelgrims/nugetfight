﻿using System;
using MongoDB.Bson;

namespace NuGetFight.Data.Models
{
    public class Fight {
        public ObjectId Id { get; set; }
        public string Query1 { get; set; }
        public string Query2 { get; set; }
        public DateTime Date { get; set; }
        public string Source { get; set; }

        public static Fight CreateFight(string query1, string query2, DateTime date, string source, string sourceUrl = "")
        {
            if (source == FightSources.MyGet)
            {
                return new MyGetFight()
                {
                    Query1 = query1,
                    Query2 = query2,
                    Date = DateTime.Now,
                    SourceUrl = sourceUrl,
                    Source = source,
                };
            }
            else
            {
                return new Fight()
                {
                    Query1 = query1,
                    Query2 = query2,
                    Date = DateTime.Now,
                    Source = source
                };
            }

        }
    }
}