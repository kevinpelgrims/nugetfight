﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NuGetFight
{
    public static class StringExtensions
    {
        public static bool CaseInsensitiveContains(this string source, string value)
        {
            return source.ToUpper().Contains(value.ToUpper());
        }
    }
}