﻿function SetSymbolSourceLinks() {
    $(".symbolsource").each(function (i, pkg) {
        var packageTitle = $(pkg).attr("data-package-title");
        var packageVersion = $(pkg).attr("data-package-version");
        $.ajax({
            url: "/Home/SymbolSourceUrl",
            data: { packageName: packageTitle, packageVersion: packageVersion }
        }).done(function (msg) {
            $(pkg).append(msg);
        });
    });
}