﻿/*globals _comma_separated_list_of_variables_*/
function drawChart(data, canvasId, chartColor1, chartColor2) {
    var graphCanvas = document.getElementById(canvasId);

    // Check if the element is available
    if (graphCanvas && graphCanvas.getContext) {
        // Open a 2D context within the canvas
        var context = graphCanvas.getContext('2d');

        // Draw the bar chart
        drawBarChart(context, data, (graphCanvas.height - 21), chartColor1, chartColor2);
    }
}

function drawBarChart(context, data, chartHeight, chartColor1, chartColor2) {
    var barChart = new BarChart(context, data, chartHeight, chartColor1, chartColor2);
    barChart.drawLines();
    barChart.drawBars(data);
    barChart.addMarkers();
}

function BarChart(context, data, chartHeight, chartColor1, chartColor2) {
    this.startX = 50;
    this.startY = 280;
    this.barWidth = 100;
       
    this.data = data;
    this.context = context;
    this.chartColor1 = chartColor1;
    this.chartColor2 = chartColor2;
    this.chartHeight = chartHeight;

    this.getMaxValue = function (data) {
        var max = parseInt(data[0].value);
        for (var i = 1; i < data.length; i++) {
            if (parseInt(data[i].value) > max) {
                max = parseInt(data[i].value);
            }
        }
        return max;
    }

    function drawLine(context, xStart, yStart, xEnd, yEnd) {
        context.beginPath();
        context.moveTo(xStart, yStart);
        context.lineTo(xEnd, yEnd);
        context.closePath();
        context.stroke();
    }

    this.drawLines = function () {
        // Draw the x and y axes
        context.lineWidth = "1.0";

        drawLine(context, this.startX, this.startY, this.startX, 30);
        drawLine(context, this.startX, this.startY, 400, this.startY);
    }

    this.drawBars = function (data) {
        // Draw the bars
        var highestValue = this.getMaxValue(data);

        context.lineWidth = "0.0";
        var bar1 = new Bar(context, this.startX, this.barWidth, this.chartHeight, this.chartColor1, highestValue, data[0], 0);
        bar1.startDraw();
        var bar2 = new Bar(context, this.startX, this.barWidth, this.chartHeight, this.chartColor2, highestValue, data[1], 1);
        bar2.startDraw();
    }

    this.addMarkers = function () {
        // Add markers to the y-axis
        var highestValue = this.getMaxValue(data);
        var markerValue = 0;
        this.context.textAlign = "right";
        this.context.fillStyle = "#000";

        var numMarkers = (highestValue < numMarkers) ? highestValue : 8;
        var markerIncrements = highestValue / numMarkers;
        
        for (var i = 0; i <= numMarkers; i++) {
            this.context.fillText(parseInt(markerValue), (this.startX - 5), (this.chartHeight - (markerValue / highestValue * 230)), 50);
            markerValue += markerIncrements;
        }
    }
}

function Bar(context, startX, barWidth, chartHeight, chartColor, highestValue, barData, i) {
    this.context = context;
    this.startX = startX;
    this.barData = barData;
    this.name = barData.keyword;
    this.height = (parseInt(barData.value) / highestValue) * 230;
    this.margin = 40;
    this.chartColor = chartColor;
    this.currentHeight = 0;
    this.barWidth = barWidth;
    this.chartHeight = chartHeight;
    this.chartNumber = i;
    this.currentHeight = 0;
    this.animationId = 0;
    // animationSteps stolen from http://www.williammalone.com/articles/html5-canvas-javascript-bar-graph/downloads/html5-canvas-bar-graph.js
    this.animationSteps = 20;
    this.numberShown = 0;

    var that = this;

    this.startDraw = function () {
        if ((this.height - this.currentHeight) < 0.01) {
            // final draw, then quit

            this.drawBar(this.height);
            this.drawBarSummary(this.barData.value);
        }
        else {

            var delta = (this.height - this.currentHeight) / this.animationSteps;
            this.currentHeight += Math.round(delta * 1000) / 1000;
            this.numberShown = this.barData.value * (this.currentHeight / this.height);


            this.drawBar(this.currentHeight);
            this.drawBarSummary(this.numberShown);


            setTimeout(function () { that.startDraw(); }, 1000 / 400);
        }
    }

    this.drawBar = function(height) {
        // draw the bar rectangle
        this.context.fillStyle = this.chartColor;
        drawRectangle(this.context, this.startX + (this.chartNumber * this.barWidth) + this.chartNumber * this.margin + this.margin, (this.chartHeight - this.currentHeight),
            this.barWidth, height, true);
    }

    this.drawBarSummary = function (number) {
        var textShown = this.name + " - " + Math.round(number);

        // Add the column title to the x-axis
        this.context.textAlign = "left";
        this.context.fillStyle = "#FFF";

        // clear previous text
        this.context.beginPath();
        this.context.rect(this.startX + (this.chartNumber * this.barWidth) + this.chartNumber * this.margin + this.margin,
                this.chartHeight + 2, this.barWidth, 10);
        this.context.closePath();
        this.context.fill();

        // redraw text
        this.context.fillStyle = "#000";
        this.context.fillText(textShown, this.startX + (this.chartNumber * this.barWidth) + this.chartNumber * this.margin + this.margin,
                this.chartHeight + 11, this.barWidth);
    }
}

function BarData(keyword, value) {
    this.keyword = keyword;
    this.value = value;
}

function drawRectangle(context, x, y, w, h, fill) {
    context.beginPath();
    context.rect(x, y, w, h);
    context.closePath();
    context.stroke();
    if (fill) context.fill();
}