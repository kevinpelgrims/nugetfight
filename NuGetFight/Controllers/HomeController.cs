﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NuGetFight.Data.Connectors;
using NuGetFight.Data.Models;
using System.Net;
using NuGetFight.Filters;
using System.Threading.Tasks;
using NuGetFight.ViewModels;
using FightConnector = NuGetFight.Data.Connectors.FightConnector;

namespace NuGetFight.Controllers
{
    public class HomeController : NuGetControllerBase
    {
        public HomeController()
        {
        }

        private List<FightSuggestion> GetSuggestedFights()
        {
            List<FightSuggestion> suggestedList = new List<FightSuggestion>();
            suggestedList.Add(new FightSuggestion("fight", "nunit", "xunit"));
            suggestedList.Add(new FightSuggestion("fight", "couch", "mongo"));
            suggestedList.Add(new FightSuggestion("fight", "ninject", "autofac"));
            suggestedList.Add(new FightSuggestion("fight", "jquery", "dojo"));

            return suggestedList;
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View(new FightInputModel() { SuggestedFights = GetSuggestedFights() });
        }

        [HttpPost]
        public ActionResult Index(FightInputModel model)
        {
            return RedirectToAction("Fight", new { @query1 = model.Query1.Trim(), @query2 = model.Query2.Trim() });
        }


        [NuGetExceptionFilter]
        [HttpGet]
        public ActionResult Fight(FightInputModel inputModel)
        {
            inputModel.SuggestedFights = GetSuggestedFights();
            return View("Fight", Fight(inputModel, NuGetConnector.NuGetConnectionString, FightSources.NuGet));
        }


        [HttpGet]
        public ActionResult NuGetNoHere()
        {
            return View();
        }

        [HttpGet]
        public ActionResult About()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Disclaimer()
        {
            return View();
        }

        [HttpGet]
        public ActionResult SymbolSourceUrl(string packageName, string packageVersion)
        {
            var symbolSourceResponse = new SymbolSourceConnector().GetSymbolSourceResponse(packageName, packageVersion);

            return symbolSourceResponse.PackageExists
                       ? (ActionResult)new ContentResult()
                            {
                                Content = string.Format(@"<a href='{0}'><img title='{1} has debugging symbols on SymbolSource.org' alt='SymbolSource url' src='{2}' /></a>",
                                            symbolSourceResponse.PackageUrl,
                                            packageName,
                                            "/content/images/symbolsource.png")
                            }
                       : (ActionResult)new EmptyResult();
        }
    }
}
