﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NuGetFight.Data;
using NuGetFight.Data.Models;
using NuGetFight.ViewModels;

namespace NuGetFight.Controllers
{
    //under construction
    public class MyGetController : NuGetControllerBase
    {
        private const string MyGetUrl = "http://www.myget.org/F/{0}";
        [HttpGet]
        public ActionResult Index()
        {
            return View(new MyGetFightInputModel());
        }

        [HttpPost]
        public ActionResult Index(MyGetFightInputModel model)
        {
            return RedirectToAction("Fight", new { @query1 = model.Query1.Trim(), @query2 = model.Query2.Trim(), @mygetFeedName = model.MyGetFeedName});
        }

        [HttpGet]
        public ActionResult Fight(MyGetFightInputModel inputModel)
        {
            return View("Fight", Fight(inputModel, string.Format(MyGetUrl,inputModel.MyGetFeedName), FightSources.MyGet, inputModel.MyGetFeedName));
        }

    }
}
