﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NuGetFight.Data;
using NuGetFight.Data.Connectors;
using NuGetFight.Data.Models;
using NuGetFight.ViewModels;

namespace NuGetFight.Controllers
{
    public class ChocolateyController: NuGetControllerBase
    {
        private List<FightSuggestion> GetSuggestedFights()
        {
            List<FightSuggestion> suggestedList = new List<FightSuggestion>();
            suggestedList.Add(new FightSuggestion("chocolatey", "git", "hg"));
            suggestedList.Add(new FightSuggestion("chocolatey", "vim", "notepad"));
            suggestedList.Add(new FightSuggestion("chocolatey", "7zip", "winrar"));

            return suggestedList;
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View(new FightInputModel() { SuggestedFights = GetSuggestedFights() });
        }

        [HttpPost]
        public ActionResult Index(FightInputModel model)
        {
            return RedirectToAction("Fight", new { @query1 = model.Query1.Trim(), @query2 = model.Query2.Trim() });
        }

        [HttpGet]
        public ActionResult Fight(FightInputModel inputModel)
        {
            inputModel.SuggestedFights = GetSuggestedFights();
            return View("Fight", Fight(inputModel, NuGetConnector.ChocolateyConnectionString, FightSources.Chocolatey));
        }
    }
}