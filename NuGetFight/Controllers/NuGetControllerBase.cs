﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NuGetFight.Data;
using NuGetFight.Data.Connectors;
using NuGetFight.ViewModels;
using FightConnector = NuGetFight.Data.Connectors.FightConnector;

namespace NuGetFight.Controllers
{
    public class NuGetControllerBase : Controller
    {
        protected FightModel Fight(FightInputModel inputModel, string connectionString, string source, string sourceUrl = "")
        {
            var nuGetConnector = new NuGetConnector(connectionString);
            var fightCounter = new FightConnector();
            fightCounter.SaveFight(inputModel.Query1, inputModel.Query2, DateTime.Now, source, sourceUrl);

            return FightModel.Create(inputModel, nuGetConnector);
        }
    }
}