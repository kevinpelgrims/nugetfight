﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NuGetFight.ViewModels
{
    public class FightSuggestion
    {
        public string Source { get; set; }
        public string Query1 { get; set; }
        public string Query2 { get; set; }

        public FightSuggestion(string source, string query1, string query2)
        {
            Source = source;
            Query1 = query1;
            Query2 = query2;
        }
    }
}