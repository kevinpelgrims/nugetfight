﻿using System.Collections.Generic;
using NuGetFight.Data.Connectors;
using NuGetFight.Data.Models;
using NuGetFight.Data;
using FightConnector = NuGetFight.Data.Connectors.FightConnector;

namespace NuGetFight.ViewModels
{
    public class FightModel
    {
        public FightInputModel FightInput { get; set; }

        public QueryResultModel Query1 { get; set; }

        public QueryResultModel Query2 { get; set; }

        public string ResultMessage { get; set; }

        public static FightModel Create(FightInputModel inputModel, NuGetConnector nuGetConnector) {
            var queryModel1 = QueryResultModel.Create(inputModel.Query1, nuGetConnector);
            var queryModel2 = QueryResultModel.Create(inputModel.Query2, nuGetConnector);

            var model = new FightModel()
            {
                FightInput = inputModel,
                Query1 = queryModel1,
                Query2 = queryModel2,
                ResultMessage = new Referee().DecideResult(queryModel1, queryModel2),
            };
            return model;
        }
    }
}