﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NuGetFight.Data;
using NuGetFight.Data.Connectors;
using NuGetFight.Data.Models;

namespace NuGetFight.ViewModels
{
    public class QueryResultModel
    {
        public string Name { get; set; }
        public int? PackageCount { get; set; }
        public int? DownloadCount { get; set; }

        public ICollection<NuGetPackage> TopPackages { get; set; }

        public static QueryResultModel Create(string query, NuGetConnector nuGetConnector)
        {
            var queryModel = new QueryResultModel() { Name = query };

            var packages = nuGetConnector.GetPackages(query);

            queryModel.PackageCount = packages.Count();
            queryModel.DownloadCount = nuGetConnector.GetDownloadCount(packages);
            queryModel.TopPackages = nuGetConnector.GetTopPackages(packages, 5);

            return queryModel;
        }
    }
}