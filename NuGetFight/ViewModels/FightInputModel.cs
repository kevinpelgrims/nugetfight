﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace NuGetFight.ViewModels
{
    public class FightInputModel
    {
        [Required(ErrorMessage = "This field is required ")]
        public String Query1 { get; set; }

        [Required(ErrorMessage = "This field is required ")]
        public String Query2 { get; set; }

        public List<FightSuggestion> SuggestedFights { get; set; }
    }
}