﻿using System;
using NuGetFight.ViewModels;

namespace NuGetFight.Data.Models
{
    public class Referee
    {
        public string DecideResult(QueryResultModel query1, QueryResultModel query2)
        {
            string victoryMessage;

            if (CalculateScore(query1) > CalculateScore(query2))
            {
                victoryMessage = query1.Name + " wins!";
            }
            else if (CalculateScore(query1) < CalculateScore(query2))
            {
                victoryMessage = query2.Name + " is victorious!";
            }
            else if (CalculateScore(query1) == 0 && CalculateScore(query2) == 0)
            {
                victoryMessage = "Nobody showed up for the fight...";
            }
            else
            {
                victoryMessage = "It's a draw. How disappointing...";
            }

            return victoryMessage;
        }

        private double CalculateScore(QueryResultModel query)
        {
            double downloadCount = Convert.ToDouble(query.DownloadCount);
            double packageCount = Convert.ToDouble(query.PackageCount);
            // If package count is 0, the whole thing is 0.
            // Dividing by 0 will return NaN, so let's not do that.
            return packageCount == 0 ? 0 : (downloadCount / packageCount) + downloadCount;
        }
    }
}