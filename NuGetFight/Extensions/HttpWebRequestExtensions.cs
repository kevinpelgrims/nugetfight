﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace NuGetFight
{
    public static class WebRequestExtensions {
        public static WebResponse EndGetResponseWithExceptionHandling(this WebRequest request, IAsyncResult asyncResult) {
            try {
                return request.EndGetResponse(asyncResult);
            }
            catch (WebException wex) {
                if (wex.Response != null) {
                    return wex.Response;
                }
                throw;
            }
        }
    }
}