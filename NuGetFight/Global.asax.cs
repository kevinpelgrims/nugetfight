﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace NuGetFight
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            // Default fight route
            routes.MapRoute(
                "Fight", // Route name
                "fight/{query1}/{query2}", // URL with parameters
                new { controller = "Home", action = "Fight" } // Parameter defaults
            );

            // Chocolatey fight route
            routes.MapRoute(
                "Chocolatey", // Route name
                "chocolatey/{query1}/{query2}", // URL with parameters
                new { controller = "Chocolatey", action = "Fight" } // Parameter defaults
            );

            // MyGet fight route
            routes.MapRoute(
                "MyGet", // Route name
                "myget/{myGetFeedName}/{query1}/{query2}", // URL with parameters
                new { controller = "MyGet", action = "Fight" } // Parameter defaults
            );

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );

            

        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);
        }
    }
}