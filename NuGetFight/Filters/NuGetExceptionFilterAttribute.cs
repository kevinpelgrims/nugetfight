﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;

namespace NuGetFight.Filters
{
    public class NuGetExceptionFilterAttribute: HandleErrorAttribute
    {
        public override void OnException(ExceptionContext filterContext)
        {
            if (filterContext.Exception is WebException)
            {
                filterContext.Result = new RedirectResult("/Home/NuGetNoHere");
                filterContext.ExceptionHandled = true;
            }
            base.OnException(filterContext);
        }
    }
}