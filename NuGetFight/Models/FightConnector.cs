﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Driver;
using System.Configuration;
using NuGetFight.Data.Models;

namespace NuGetFight.Models
{
    public class FightConnector
    {
        private MongoServer server;
        private MongoDatabase mongoDb;
        private MongoCollection<Fight> fightCollection;

        public FightConnector()
        {
            server = MongoServer.Create(ConfigurationManager.ConnectionStrings["MongoLab"].ToString());
            server.Connect();

            mongoDb = server.GetDatabase(ConfigurationManager.AppSettings["MongoDBName"].ToString());
        }

        public int GetBodyCount()
        {
            return Convert.ToInt32(mongoDb.GetCollection<Fight>("fights").Count());
        }

        public string GetMostSearchedPackage()
        {
            fightCollection = mongoDb.GetCollection<Fight>("fights");

            if (fightCollection.Count().Equals(0))
            {
                return String.Empty;
            }

            var fights = fightCollection.FindAll();
            var allQueries = fights.Select(f => f.Query1).ToList();
            allQueries.AddRange(fights.Select(f => f.Query2));
            var groupedFights = allQueries.GroupBy(f => f).Select(g => new FightCount(g.Key, g.Count()));
            return groupedFights.OrderByDescending(item => item.Count).First().Query;
        }

        public void SaveFight(string query1, string query2, DateTime date, string source, string sourceUrl = "")
        {
            fightCollection = mongoDb.GetCollection<Fight>("fights");
            fightCollection.Insert(CreateFight(query1, query2, date, source, sourceUrl));
        }

        private Fight CreateFight(string query1, string query2, DateTime date, string source, string sourceUrl = "")
        {
            if (source == FightSources.MyGet)
            {
                return new MyGetFight()
                           {
                               Query1 = query1,
                               Query2 = query2,
                               Date = DateTime.Now,
                               SourceUrl = sourceUrl,
                               Source = source,
                           };
            }
            else
            {
                return new Fight()
                           {
                               Query1 = query1,
                               Query2 = query2,
                               Date = DateTime.Now,
                               Source = source
                           };
            }

        }
    }
}