﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Driver;
using System.Configuration;

namespace NuGetFight.Models
{
    public class FightCounter
    {
        MongoServer server;
        MongoDatabase mongoDb;
        MongoCollection<Fight> fightCollection;
        
        public FightCounter() {
            server = MongoServer.Create(ConfigurationManager.ConnectionStrings["MongoLab"].ToString());
            server.Connect();

            mongoDb = server.GetDatabase(ConfigurationManager.AppSettings["MongoDBName"].ToString());
        }

        public string GetMostSearchedPackage()
        {
            fightCollection = mongoDb.GetCollection<Fight>("fights");

            if (fightCollection.Count().Equals(0))
            {
                return String.Empty;
            }

            var fights = fightCollection.FindAll();
            var allQueries = fights.Select(f => f.Query1).ToList();
            allQueries.AddRange(fights.Select(f => f.Query2));
            var groupedFights = allQueries.GroupBy(f => f).Select(g => new FightCount(g.Key, g.Count()));
            return groupedFights.OrderByDescending(item => item.Count).First().Query;
        }

        public void AddOne(string query1, string query2, DateTime date) 
        {
            fightCollection = mongoDb.GetCollection<Fight>("fights");
            fightCollection.Insert(new Fight()
            {
                Query1 = query1,
                Query2 = query2,
                Date = DateTime.Now,
            });
        }
    }
}